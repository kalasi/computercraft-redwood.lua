[FOR THE MINECRAFT MOD "COMPUTERCRAFT"]

# ComputerCraft Redwood Script

Chops down a redwood tree, then dumps it into a chest behind it.

### Installation

Get the github-retrieval script via `pastebin get wPtGKMam`, then
get this script via: `github taiyaeix computercraft-redwooder.lua`

### Operation

Set your Turtle as such:

```
KEY:
S = Sapling
T = Turtle
C = Chest

SS
SS
T
C
```

In item 1 place a stack of fuel, and as it needs fuel, it will use it.


### License

Check the LICENSE.md file. In short, it's Unlicensed, do whatever with it.
