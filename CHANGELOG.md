# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).



## [Unreleased]

### Changed

### Added



## [1.0.2] - 2016-01-19

### Changed
- Fixed version tag from 1.0.0 to 1.0 in changelog
- Fixed compare links in changelog

### Added



## [1.0.1] - 2016-01-18

### Changed

### Added
- Lots of documentation to the script
- Picture in script of how to setup the turtle



## 1.0.0 - 2016-01-04

Initial release.



[Unreleased]: https://github.com/taiyaeix/computercraft-redwooder.lua/compare/v1.0.2...HEAD
[1.0.2]: https://github.com/taiyaeix/computercraft-redwooder.lua/compare/v1.0.1...v1.0.2
[1.0.1]: https://github.com/taiyaeix/computercraft-redwooder.lua/compare/v1.0...v1.0.1
