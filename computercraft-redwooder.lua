-- This is free and unencumbered software released into the public domain.
--
-- Anyone is free to copy, modify, publish, use, compile, sell, or distribute
-- this software, either in source code form or as a compiled binary, for any
-- purpose, commercial or non-commercial, and by any means.
--
-- In jurisdictions that recognize copyright laws, the author or authors of this
-- software dedicate any and all copyright interest in the software to the
-- public domain. We make this dedication for the benefit of the public at large
-- and to the detriment of our heirs and successors. We intend this dedication
-- to be an overt act of relinquishment in perpetuity of all present and future
-- rights to this software under copyright law.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
-- ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- For more information, please refer to http://unlicense.org


-- The turtle needs to be positioned in a specific way to function correctly.
-- This text picture is from looking above down to the turtle, chest, and
-- saplings.
--
-- Picture Key:
-- S = Sapling
-- T = Turtle
-- C = Chest
--
-- Picture:
--
-- SS
-- SS
-- T
-- C

-- If the fuel level is below 230 (can move 230 blocks), then select the item in
-- the first inventory slot and consume it. This should be logs, so it's
-- basically self-sustainable.
function needFuel()
  if turtle.getFuelLevel() <= 230 then
    turtle.select(1)
    turtle.refuel(1)
  end
end --needFuel()

-- Digs one block above, moves to that block, then digs one block forward.
function digUp()
  turtle.digUp()
  turtle.up()
  turtle.dig()
end --digUp()

-- Digs one block below, moves to that block, then digs one block forward.
function digDown()
  turtle.digDown()
  turtle.down()
  turtle.dig()
end --digDown()

-- Loops through each inventory slot 1 through 16 and drops it in front of the
-- turtle. Ideally, it should be facing a chest to drop the items into.
function emptyInventory()
  for i = 1, 16 do
    turtle.select(i)
    turtle.drop()
  end -- for
end --emptyInventory()

-- Go up and fell in front+above

-- Dig the block in front of the turtle, so that it can place itself in the
-- bottom-left-most quadrant of the 2x2 tree.
turtle.dig()

-- Place self in bottom-left-most quadrant of tree.
turtle.forward()

-- Dig forward, then start looping over the digUp() function.
turtle.dig()

-- Set the current Y coordinate as the height. Since it can't actually detect
-- the actual Y coordinate in MC terms, set the current Y coordinate as
-- relative to 0.
--
-- If the turtle is at a Y coordinate of 80, then the relative height is 0. If
-- it digs 100 blocks up, then when coming back down it will know to go back
-- down 100 blocks, making its height relatively 0, back to 80.
local height = 0

-- Continue to keep going up until the last log/leaves is mined.
while turtle.detectUp() do
  digUp()

  -- Increment the height gone by 1.
  -- This is used when going down so
  -- that the number of blocks gone
  -- down is the same number as the
  -- amount gone up.
  height = height + 1

  -- Check if fuel is needed.
  needFuel()
end -- detecting up

-- Position self on the right-side of the tree now, instead of the left since
-- the left side has been entirely felled.
turtle.turnRight()
turtle.dig()
turtle.forward()
turtle.turnLeft()
turtle.dig()

-- Going back to the topic of height, decrement the height until it is back to
-- 0, felling along the way. If the relative height is 100, then it will count
-- back down to 0.
while height > 0 do
  digDown()

  height = height - 1

  -- Check if fuel is needed.
  needFuel()
end -- going back down same height

-- Position self to dump the contents of the inventory into the chest that
-- should be placed behind the turtle when it start this script.
turtle.turnLeft()
turtle.forward()
turtle.turnLeft()
turtle.forward()

-- empty the inventory slots 1-16
emptyInventory()

-- Rotate 180 degrees so that it is facing the same way in the original
-- position.
turtle.turnLeft()
turtle.turnLeft()
